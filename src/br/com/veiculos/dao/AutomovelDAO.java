package br.com.veiculos.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.veiculos.bean.Automovel;

public class AutomovelDAO {

	private static AutomovelDAO instance;
	protected EntityManager entityManager;

	public static AutomovelDAO getInstance() {
		if (instance == null) {
			instance = new AutomovelDAO();
		}

		return instance;
	}

	public AutomovelDAO() {
	                   entityManager = getEntityManager();
	         }

	private EntityManager getEntityManager() {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("trabalho_pos");
		if (entityManager == null) {
			entityManager = factory.createEntityManager();
		}

		return entityManager;
	}

	public Automovel getById(final int id) {
		return entityManager.find(Automovel.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Automovel> findAll() {
		return entityManager.createQuery("FROM " + Automovel.class.getName()).getResultList();
	}

	public void persist(Automovel Automovel) {
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(Automovel);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void merge(Automovel Automovel) {
		try {
			entityManager.getTransaction().begin();
			entityManager.merge(Automovel);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void remove(Automovel Automovel) {
		try {
			entityManager.getTransaction().begin();
			Automovel = entityManager.find(Automovel.class, Automovel.getId());
			entityManager.remove(Automovel);
			entityManager.getTransaction().commit();
		} catch (Exception ex) {
			ex.printStackTrace();
			entityManager.getTransaction().rollback();
		}
	}

	public void removeById(final int id) {
		try {
			Automovel Automovel = getById(id);
			remove(Automovel);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
