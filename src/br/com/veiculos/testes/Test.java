package br.com.veiculos.testes;

import java.util.ArrayList;
import java.util.List;

import br.com.veiculos.bean.Automovel;
import br.com.veiculos.bean.Bicicleta;
import br.com.veiculos.bean.Veiculo;
import br.com.veiculos.dao.*;

class Test {
	public static void main(String args[]) {
		
		Automovel a = new Automovel();
		a.setMarca("Honda");
		a.setModelo("NX150");
		a.setDataLimpeza("25/05/2018");
		a.setDataManutencao("26/05/2018");
		a.setDataTrocaOleo("27/05/2018");
		a.setDataVerificacao("30/05/2018");
		
		AutomovelDAO.getInstance().merge(a);
		
		Bicicleta b = new Bicicleta();
		b.setMarca("Monark");
		b.setModelo("Barra forte");
		b.setDataLimpeza("12/06/2018");
		b.setDataManutencao("14/06/2018");
		
		BicicletaDAO.getInstance().merge(b);
		
		Automovel a1 = new Automovel();
		a1.setMarca("Honda");
		a1.setModelo("NX150");
		a1.setDataLimpeza("25/05/2018");
		a1.setDataManutencao("26/05/2018");
		a1.setDataTrocaOleo("27/05/2018");
		a1.setDataVerificacao("30/05/2018");
		
		AutomovelDAO.getInstance().merge(a1);
		
		Bicicleta b1 = new Bicicleta();
		b1.setMarca("Monark");
		b1.setModelo("Barra forte");
		b1.setDataLimpeza("12/06/2018");
		b1.setDataManutencao("14/06/2018");
		
		BicicletaDAO.getInstance().merge(b1);
				
		Oficina o = new Oficina();
		List<Automovel> automovel = new ArrayList<Automovel>();
		automovel = AutomovelDAO.getInstance().findAll();
		
		for (Veiculo v : automovel) {
			v = o.proximo();
			o.manutencao(v);
		}
	}
}