package br.com.veiculos.model;

public class Automovel extends Veiculo {
	public Automovel() {
		System.out.println("Automovel");
	}

	public void listarVerificacoes() {
		System.out.println("Automovel.listarVerificacoes ");
	}

	public void ajustar() {
		System.out.println("Automovel.ajustar ");
	}

	public void limpar() {
		System.out.println("Automovel.limpar ");
	}

	public void mudarOleo() {
		System.out.println("Automovel.mudouOleo");
	}

	@Override
	public void listaVerificacoes() {
		// TODO Auto-generated method stub
		
	}
}