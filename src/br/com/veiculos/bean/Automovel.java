package br.com.veiculos.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "automovel")
public class Automovel extends Veiculo {
	public Automovel() {
		System.out.println("Automovel");
	}

	@Id
	private int id;

	@Column
	private String marca;

	@Column
	private String modelo;

	@Column
	private String placa;

	@Column
	private String dataVerificacao;

	@Column
	private String dataManutencao;

	@Column
	private String dataLimpeza;

	@Column
	private String dataTrocaOleo;

	public void ajustar() {
		System.out.println("Automovel.ajustar ");
	}

	public void limpar() {
		System.out.println("Automovel.limpar ");
	}

	public void mudarOleo() {
		System.out.println("Automovel.mudouOleo");
	}

	@Override
	public void listaVerificacoes() {
		System.out.println("Automovel.listarVerificacoes ");

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getDataVerificacao() {
		return dataVerificacao;
	}

	public void setDataVerificacao(String dataVerificacao) {
		this.dataVerificacao = dataVerificacao;
	}

	public String getDataManutencao() {
		return dataManutencao;
	}

	public void setDataManutencao(String dataManutencao) {
		this.dataManutencao = dataManutencao;
	}

	public String getDataLimpeza() {
		return dataLimpeza;
	}

	public void setDataLimpeza(String dataLimpeza) {
		this.dataLimpeza = dataLimpeza;
	}

	public String getDataTrocaOleo() {
		return dataTrocaOleo;
	}

	public void setDataTrocaOleo(String dataTrocaOleo) {
		this.dataTrocaOleo = dataTrocaOleo;
	}
}