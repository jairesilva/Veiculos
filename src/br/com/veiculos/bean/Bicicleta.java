package br.com.veiculos.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bicicleta")
public class Bicicleta extends Veiculo {
	public Bicicleta() {
		System.out.println("Bicicleta");
	}
	
	@Id
	private int id;

	@Column
	private String marca;
	
	@Column
	private String modelo;

	@Column
	private String dataManutencao;

	@Column
	private String dataLimpeza;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getDataManutencao() {
		return dataManutencao;
	}

	public void setDataManutencao(String dataManutencao) {
		this.dataManutencao = dataManutencao;
	}

	public String getDataLimpeza() {
		return dataLimpeza;
	}

	public void setDataLimpeza(String dataLimpeza) {
		this.dataLimpeza = dataLimpeza;
	}

	public void ajustar() {
		System.out.println("Bicicleta.ajustar");
	}

	public void limpar() {
		System.out.println("Bicicleta.limpar");
	}

	@Override
	public void listaVerificacoes() {
		System.out.println("Bicicleta.listarVerificacoes ");
	}
}