-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema trabalho_pos
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema trabalho_pos
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `trabalho_pos` DEFAULT CHARACTER SET utf8 ;
USE `trabalho_pos` ;

-- -----------------------------------------------------
-- Table `trabalho_pos`.`automovel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `trabalho_pos`.`automovel` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `marca` VARCHAR(45) NULL,
  `modelo` VARCHAR(45) NULL,
  `placa` VARCHAR(7) NULL,
  `dataVerificacao` VARCHAR(10) NULL,
  `dataManutencao` VARCHAR(10) NULL,
  `dataLimpeza` VARCHAR(10) NULL,
  `dataTrocaOleo` VARCHAR(10) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `trabalho_pos`.`bicicleta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `trabalho_pos`.`bicicleta` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `marca` VARCHAR(45) NULL,
  `modelo` VARCHAR(45) NULL,
  `dataManutencao` VARCHAR(10) NULL,
  `dataLimpeza` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
